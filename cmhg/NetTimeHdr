; Copyright 1996 Acorn Computers Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;
;
; cmhg header file for the NetTime module
;
#include "VersionNum"
#include "Global/Services.h"
#include "Global/RISCOS.h"

title-string:           NetTime
help-string:            NetTime Module_MajorVersion_CMHG Module_MinorVersion_CMHG
date-string:            Module_Date_CMHG
international-help-file: "Resources:$.Resources.NetTime.Messages"

initialisation-code:    module_init
finalisation-code:      module_final

vector-handlers:        module_osword15entry/module_osword15handler

service-call-handler:   module_service Service_PostInit Service_PreReset Service_ShutDown,
                        Service_InternetStatus Service_Dialler Service_NCRegistry

event-handler:          module_evententry/module_eventhandler Event_Internet

generic-veneers:        module_timerentry/module_timerhandler, module_callentry/module_callhandler

swi-chunk-base-number:  0x54ac0

swi-handler-code:       module_swi_handler

swi-decoding-table:     NetTime,
                        Status,
                        States

command-keyword-table:  module_cmd_handler

NetTime_Kick(min-args:0, max-args:0, international:,
             help-text:"HNTMKIC", invalid-syntax: "SNTMKIC"),
NetTime_Status(min-args:0, max-args:0, international:,
             help-text:"HNTMSTA", invalid-syntax: "SNTMSTA"),
NetTime_PollInterval(min-args:1, max-args:1, international:,
             help-text:"HNTMPLI", invalid-syntax: "SNTMPLI")
