; Copyright 1996 Acorn Computers Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;
        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:Machine.<Machine>
        GET     Hdr:APCS.<APCS>
        GET     Hdr:System

	AREA |C$$Code|, CODE, READONLY

        EXPORT  ntp_add[LEAF]
ntp_add
        LDR     ip,[sp,#0]      ; ip := b.frac
        ADDS    ip,a3,ip        ; ip := a.frac + b.frac
        STR     ip,[a1,#4]      ; output->frac := ip
        ADC     ip,a2,a4        ; ip := a.secs + b.secs with carry
        STR     ip,[a1,#0]      ; output->secs := ip
        Return  ,LinkNotStacked

        EXPORT  ntp_sub[LEAF]
ntp_sub
        LDR     ip,[sp,#0]      ; ip := b.frac
        SUBS    ip,a3,ip        ; ip := a.frac - b.frac
        STR     ip,[a1,#4]      ; output->frac := ip
        SBC     ip,a2,a4        ; ip := a.secs - b.secs with carry
        STR     ip,[a1,#0]      ; output->secs := ip
        Return  ,LinkNotStacked

        IMPORT  module_osword15entry
        EXPORT  wordv_veneer
wordv_veneer
        TEQ     r0,#15                  ; fast check for OS_Word 15
        MOVNE   pc,lr
        B       module_osword15entry    ; if it is, then on to CMHG veneer


        EXPORT  get_t0_count
get_t0_count
        MOV     ip, lr
        [ {CONFIG}<>26
        MRS     a3, CPSR
        ]
        SWI     XOS_EnterOS             ; Must be in SVC mode to peek IOC/IOMD
        MOV     a4,#IOC
; Shut off interrupts (briefly) to ensure an atomic read of these
; silly hardware registers.
        [ {CONFIG}=26
        TEQP    pc,#I_bit :OR: SVC_mode
        |
        MRS     a1, CPSR
        ORR     a1, a1, #I32_bit
        MSR     CPSR_c, a1              ; Disable interrupts
        ]
        STRB    a4,[a4,#Timer0LR]
        LDRB    a2,[a4,#Timer0CL]
        LDRB    a1,[a4,#Timer0CH]
        ORR     a1,a2,a1,LSL #8
        [ {CONFIG}<>26
        MSR     CPSR_c, a3              ; Restore entry mode and irq state
        ]
        Return  ,LinkNotStacked,,ip


sb      RN      9
SB      RN      9

; unsigned long _get_hal_t0_count(uint32_t hal_sb, void * hal_fn_ptr);
;
; Call HAL_TimerReadCountdown to get current Timer 0 countdown value.
;
;   Static base value in a1
;   Pointer to HAL_TimerReadCountdown routine in a2

        EXPORT  get_hal_t0_count
get_hal_t0_count   ROUT
; Are we in USR mode?
        [ {CONFIG} = 26
        TST     lr, #3                  ; Is mode USR26?
        |
        MRS     a3, CPSR
        TST     a3, #3                  ; Is mode USR26 or USR32?
        ]
        BEQ     %FT50

; We are in SVC mode already
        FunctionEntry   sb
        MOV     sb, a1
        MOV     a1, #0                  ; We want to read timer 0
        ADR     lr, %FT01
        MOV     pc, a2                  ; Call HAL
01
        Return  sb

50
; We are in USR mode
        [ {CONFIG} = 26
        MOV     ip, lr
        ]
        SWI     XOS_EnterOS             ; Enter SVC mode
        [ {CONFIG} = 26
        STMFD   sp!,{sb, ip}
        |
        STMFD   sp!,{a3, sb}
        ]
        MOV     sb, a1
        MOV     a1, #0                  ; We want to read timer 0
        ADR     lr, %FT51
        MOV     pc, a2                  ; Call HAL
51
        [ {CONFIG} = 26
        Return  sb                      ; Return and restore entry mode
        |
        LDMFD   sp!,{a3, sb}
        MSR     CPSR_c, a3              ; Restore entry mode
        Return  ,LinkNotStacked
        ]

	END
